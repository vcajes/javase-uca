package py.edu.uca.javase.ejercicios;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.WindowConstants;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.sql.PreparedStatement;

import net.miginfocom.swing.MigLayout;
import py.edu.uca.javase.clase07.MyTableModel;

public class Clase9EJ1 {

	public static JLabel labelNombre = new JLabel("Nombre");
	public static JTextField txtNombre = new JTextField();
	public static JLabel labelApellido = new JLabel("Apellido");
	public static JTextField txtApellido = new JTextField();
	public static JLabel labelSexo = new JLabel("Sexo");
	public static JComboBox<String> cbSexo = new JComboBox<String>();
	public static JCheckBox chkReligion = new JCheckBox("Religión Católica?");
	public static JButton botonAgregar = new JButton("Agregar Persona");
	public static JButton botonEditar = new JButton("Editar Persona");
	public static JButton botonBorrar = new JButton("Borrar Persona");
	public static JLabel labelFiltro = new JLabel("Filtro de búsqueda");
	public static JTextField txtFiltro = new JTextField();
	public static JButton botonFiltrar = new JButton("Buscar");
	public static JFrame frame = new JFrame();
	
	
	public static MyTableModel modeloTabla = new MyTableModel(); //creamos un modelo de tabla generico
	public static JTable tabla = new JTable(modeloTabla); //creamos la tabla con nuestro modelo de tabla
	public static JScrollPane scrollPane = new JScrollPane(tabla); //creamos un panel con scroll y ponemos adentro la tabla
	
	public static int idPersonaSel = -1;
	
	private static Connection conn = null;
		
	public static void conectarDB(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/prueba?"
					+ "user=root&password=CATTLE14050514&useUnicode=true&characterEncoding=utf-8");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		//creamos un JFrame
		frame.getContentPane().setLayout(new MigLayout());
		frame.setSize(new Dimension(500, 500));
		frame.setLocation(100, 100);
		frame.setResizable(true);
		frame.setTitle("Clase 9 - Ejercicio 1");
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		
		botonEditar.setEnabled(false);
		botonBorrar.setEnabled(false);
		
		//cargamos los elementos al combo
		cbSexo.addItem("Masculino");
		cbSexo.addItem("Femenino");
		cbSexo.setSelectedIndex(0);

		modeloTabla.addColumn("Nº"); //agregamos la 1ra columna
		modeloTabla.addColumn("Nombre"); //agregamos la 2da columna
		modeloTabla.addColumn("Apellido"); //agregamos la 3ra columna
		modeloTabla.addColumn("Sexo"); //agregamos la 4ta columna
		modeloTabla.addColumn("Catolico"); //agregamos la 5ta columna
		
		//capturamos los eventos
		botonAgregar.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				int dialogResult = JOptionPane.showConfirmDialog (null, "Estas seguro de querer agregar el registro?","Atencion!", JOptionPane.YES_NO_OPTION);
				if(dialogResult == JOptionPane.YES_OPTION){
					int rowsAffected = -1;
					try {
						PreparedStatement statement = conn.prepareStatement("INSERT INTO personas (nombre, apellido, sexo, catolico) VALUES (?,?,?,?)");
						statement.setString(1, txtNombre.getText());
						statement.setString(2, txtApellido.getText());
						statement.setString(3, (String)cbSexo.getSelectedItem());
						statement.setBoolean(4, chkReligion.isSelected());
						rowsAffected = statement.executeUpdate();
						statement.close();
					} catch (SQLException ex) {
						ex.printStackTrace();
					}	
					
					mostrarPersonasEnTabla();
					
					if(rowsAffected == 1)
						JOptionPane.showMessageDialog(null, "Se inserto correctamente la persona.",
							"OK", JOptionPane.INFORMATION_MESSAGE);
					else
						JOptionPane.showMessageDialog(null, "Error al intentar insertar el registro de la DB.",
								"ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
		botonBorrar.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				int dialogResult = JOptionPane.showConfirmDialog (null, "Estas seguro de querer eliminar el registro?","Atencion!", JOptionPane.YES_NO_OPTION);
				if(dialogResult == JOptionPane.YES_OPTION){
					int rowsAffected = -1;
					try {
						PreparedStatement statement = conn.prepareStatement("DELETE FROM personas WHERE id = ?");
						statement.setInt(1, idPersonaSel);
						rowsAffected = statement.executeUpdate();
						statement.close();
					} catch (SQLException ex) {
						ex.printStackTrace();
					}	
					
					mostrarPersonasEnTabla();
					
					if(rowsAffected == 1)
						JOptionPane.showMessageDialog(null, "Se borro correctamente la persona.",
							"OK", JOptionPane.INFORMATION_MESSAGE);
					else
						JOptionPane.showMessageDialog(null, "Error al intentar borrar el registro de la DB.",
								"ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
		botonFiltrar.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				buscarPersonas();
			}
		});
		
		botonEditar.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				int dialogResult = JOptionPane.showConfirmDialog (null, "Estas seguro de querer editar el registro?","Atencion!", JOptionPane.YES_NO_OPTION);
				if(dialogResult == JOptionPane.YES_OPTION){
					int rowsAffected = -1;
					try {
						PreparedStatement statement = conn.prepareStatement("UPDATE personas SET nombre = ?, apellido = ?, sexo = ?, catolico = ? WHERE id = ?");
						statement.setString(1, txtNombre.getText());
						statement.setString(2, txtApellido.getText());
						statement.setString(3, (String)cbSexo.getSelectedItem());
						statement.setBoolean(4, chkReligion.isSelected());
						statement.setInt(5, idPersonaSel);
						rowsAffected = statement.executeUpdate();
						statement.close();
					} catch (SQLException ex) {
						ex.printStackTrace();
					}	
					
					mostrarPersonasEnTabla();
					
					if(rowsAffected == 1)
						JOptionPane.showMessageDialog(null, "Se modifico correctamente la persona.",
							"OK", JOptionPane.INFORMATION_MESSAGE);
					else
						JOptionPane.showMessageDialog(null, "Error al intentar insertar el registro de la DB.",
								"ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
		tabla.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				int fila = tabla.rowAtPoint(e.getPoint());
				
				//copiamos los valores a la interfaz
				txtNombre.setText((String)tabla.getValueAt(fila,1));
				txtApellido.setText((String)tabla.getValueAt(fila,2));
				cbSexo.setSelectedItem((String)tabla.getValueAt(fila,3));
				chkReligion.setSelected((boolean)tabla.getValueAt(fila,4));
				
				//guardamos el ID de la persona seleccionada
				idPersonaSel = (int) tabla.getValueAt(fila,0);
				
				//habilitamos los botones para editar y borrar
				botonEditar.setEnabled(true);
				botonBorrar.setEnabled(true);
			}});
		
		//agregamos el ordenador por columna
		RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(modeloTabla);
        tabla.setRowSorter(sorter);
		
        frame.add(labelNombre);
        frame.add(txtNombre, "gapleft 15px, spanx, growx, wrap");
        frame.add(labelApellido);
        frame.add(txtApellido, "gapleft 15px, w 200:5000:5000, growx, wrap");
        frame.add(labelSexo);
        frame.add(cbSexo, "gapleft 15px, w 200:5000:5000, growx, wrap");
        frame.add(chkReligion, "spanx, growx, wrap");
        frame.add(botonAgregar, "spanx, split 3, growx");
        frame.add(botonEditar, "growx");
        frame.add(botonBorrar, "growx, wrap");
		frame.add(scrollPane, "width 100:5000:5000, height 100:5000:5000, grow, span, wrap");
		frame.add(labelFiltro, "split 3, spanx");
        frame.add(txtFiltro, "gapleft 15px, grow");
        frame.add(botonFiltrar, "width 120:120:120, wrap");
        
		frame.setVisible(true); //mostramos el frame
		
		conectarDB();
		mostrarPersonasEnTabla();
	}
	
	public static void mostrarPersonasEnTabla(){
		//eliminamos todo lo que hay en la tabla
		botonEditar.setEnabled(false);
		botonBorrar.setEnabled(false);
		
		while(modeloTabla.getRowCount() > 0)
			modeloTabla.removeRow(0);
		
		try {
			PreparedStatement statement = conn.prepareStatement("SELECT * FROM personas");
			ResultSet resultSet = statement.executeQuery(); //consultamos
			while(resultSet.next()){ //iteramos fila por fila
				Integer id = resultSet.getInt("id");
				String nombre = resultSet.getString("nombre");
				String apellido = resultSet.getString("apellido");
				String sexo = resultSet.getString("sexo");
				Boolean catolico = resultSet.getBoolean("catolico");
				
				modeloTabla.addRow(new Object[]{id,	nombre,	apellido, sexo,	catolico});
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		limpiarCampos();
	}
	
	public static void buscarPersonas(){
		//eliminamos todo lo que hay en la tabla
		botonEditar.setEnabled(false);
		botonBorrar.setEnabled(false);
		
		while(modeloTabla.getRowCount() > 0)
			modeloTabla.removeRow(0);
		
		try {
			PreparedStatement statement = conn.prepareStatement("SELECT * FROM personas WHERE nombre LIKE ?");
			statement.setString(1, "%"+txtFiltro.getText()+"%");
			ResultSet resultSet = statement.executeQuery(); //consultamos
			while(resultSet.next()){ //iteramos fila por fila
				Integer id = resultSet.getInt("id");
				String nombre = resultSet.getString("nombre");
				String apellido = resultSet.getString("apellido");
				String sexo = resultSet.getString("sexo");
				Boolean catolico = resultSet.getBoolean("catolico");
				
				modeloTabla.addRow(new Object[]{id,	nombre,	apellido, sexo,	catolico});
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		limpiarCampos();
	}
	
	public static void limpiarCampos(){
		txtNombre.setText("");
		txtApellido.setText("");
		cbSexo.setSelectedIndex(0);
		chkReligion.setSelected(false);
	}
}
