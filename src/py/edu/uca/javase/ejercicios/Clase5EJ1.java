package py.edu.uca.javase.ejercicios;

import java.io.File;

public class Clase5EJ1 {

	public static void main(String[] args) {
		if(args.length == 0)
			args = new String[] {"C:\\"};
		
		try{
			File pathName = new File(args[0]);
			String[] fileNames = pathName.list();
			if(fileNames != null){
				for(String fname : fileNames){
					File f = new File(pathName.getPath(), fname);
					if(f.isDirectory()){
						main(new String[]{ f.getPath() });
					}else if(f.isFile()){
						if(f.getCanonicalPath().endsWith(".txt")){
							System.out.println(f.getCanonicalPath());
						}
					}
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
}
