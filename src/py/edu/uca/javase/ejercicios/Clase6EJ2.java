package py.edu.uca.javase.ejercicios;


public class Clase6EJ2 {

	static Clase6EJ2 c6 = null;
	int pos = 0;
	int[] valores = new int[100];
	static int cant = 0;
	
	public static void main(String args[]){
		c6 = new Clase6EJ2();
		Thread ha = new Thread(c6.new HiloAdd());
		Thread hp = new Thread(c6.new HiloPrint());
		ha.start();
		hp.start();
		
		while(cant<20)
			continue;
	}
	
	public synchronized void add(){
		while(pos >= 100)
			try { wait(); } catch (InterruptedException e) {e.printStackTrace();}
		int addCant = (int) (Math.random()*2);
		System.out.println("se van a agregar " + addCant + " elementos");
		for(int i=0; i<addCant; i++){
			int valor = (int) (Math.random() * 100);
			valores[pos] = valor;
			pos++;
		}
		notifyAll();
		try { Thread.sleep(((long)Math.random())*2000 + 2000); } catch (InterruptedException e) {e.printStackTrace();}
	}
	
	public synchronized void print(){
		System.out.println("print() llamado");
		while(pos <= 0)
			try { wait(); } catch (InterruptedException e) {}
		System.out.println("Imprimir el valor: " + valores[pos-1]);
		pos--;
		cant++;
		notifyAll();
		try { Thread.sleep(((long)Math.random())*2000 + 2000); } catch (InterruptedException e) {}
	}
	
	private class HiloAdd implements Runnable{
		public void run() {
			while(cant < 20){
				c6.add();
			}			
		}
		
	}
	
	private class HiloPrint implements Runnable{
		public void run() {
			while(cant < 20){
				c6.print();
			}			
		}
		
	}
}
