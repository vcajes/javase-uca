package py.edu.uca.javase.ejercicios;

public class Clase4EJ1 {

	public static void main(String[] args) {
		Clase4EJ1 main = new Clase4EJ1();
		TuplaTriple<String, Integer, Double> tt1 = main.new TuplaTriple<String, Integer, Double>("TT1", 1, 4.5);
		TuplaTriple<String, Integer, Double> tt2 = main.new TuplaTriple<String, Integer, Double>("TT1", 1, 4.5);
		System.out.println(tt1);
		System.out.println(tt2);
		System.out.println(tt1.equals(tt2) ? "TT1 es igual a TT2" : "TT1 y TT2 son distintos");
		
		tt2.setPrimero("TT2");
		System.out.println(tt2);
		System.out.println(tt1.equals(tt2) ? "TT1 es igual a TT2" : "TT1 y TT2 son distintos");
	}
	
	public class TuplaTriple<A,B,C>{
		private A primero;
		private B segundo;
		private C tercero;
		
		public TuplaTriple(){ this.primero = null; this.segundo = null; this.tercero = null; }
		public TuplaTriple(A p, B s, C t){ primero = p; segundo = s; tercero = t; }
	
		public A getPrimero(){ return this.primero; }
		public void setPrimero(A primero){ this.primero = primero;}
		public B getSegundo(){ return this.segundo; }
		public void setSegundo(B segundo){ this.segundo = segundo;}
		public C getTercero(){ return this.tercero; }
		public void setTercero(C tercero){ this.tercero = tercero;}
		
		@Override
		public String toString(){
			return String.format("Primero: %s | Segundo: %s | Tercero %s",
					primero.toString(), segundo.toString(), tercero.toString());
		}
		
		
		@SuppressWarnings("unchecked")
		@Override
		public boolean equals(Object obj){
			try{
				if(obj instanceof TuplaTriple){
					TuplaTriple<A, B, C> tt = (TuplaTriple<A, B, C>) obj;
					if(tt.primero.equals(this.primero) && tt.segundo.equals(this.segundo) && tt.tercero.equals(this.tercero))
						return true;
				}
			}catch(Exception e){}
			return false;
		}
	}
}
