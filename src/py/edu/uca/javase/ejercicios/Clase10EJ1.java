package py.edu.uca.javase.ejercicios;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.logging.log4j.*;

public class Clase10EJ1 {

	private static Logger log = LogManager.getLogger(Clase10EJ1.class);
	
	public static void main(String[] args) {
		
		//lista de locales para Java 7
		//http://www.oracle.com/technetwork/java/javase/javase7locales-334809.html
		
		Locale[] locales = new Locale[]{
			new Locale("en", "US"), //ingles estados unidos
			new Locale("es"), //español
			new Locale("ru", "RU"), //ruso de rusia
			new Locale("de", "DE"), //aleman de alemania
			new Locale("fr", "FR") //frances de francia	
		};
		
		for(int i=0; i<locales.length; i++){
			ResourceBundle bundle = ResourceBundle.getBundle("i18n.strings", locales[i]);
			NumberFormat currency = NumberFormat.getCurrencyInstance(locales[i]);
			
			log.debug(String.format("%s --> %s",
					locales[i].getDisplayName(), bundle.getString("jungle")));
			
			log.debug(String.format("%s --> %s %s", 
					locales[i].getDisplayName(), bundle.getString("price"), currency.format(5000.0)));
		}
	}
}


