package py.edu.uca.javase.ejercicios;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Locale;

public class Clase4EJ2 {

	public static void main(String[] args) {
		String testStr = "test";
		long startTime;
		int i;
		int cantPruebas = 100000;
		
		Locale localeES = new Locale("es");
		NumberFormat numberFormatES = NumberFormat.getInstance(localeES);
		
		HashSet<String> hashSet = new HashSet<String>();
		HashMap<String, String> hashMap = new HashMap<String, String>();
		ArrayList<String> arrayList = new ArrayList<String>();
		LinkedList<String> linkedList = new LinkedList<String>();
		
		System.out.println("--- HASHSET ---");
		
		startTime = System.nanoTime();
		for(i=0; i<cantPruebas; i++)
			hashSet.add("test"+i);
		System.out.println(String.format("Agregar %d elementos tardo %s nanosegundos", cantPruebas, numberFormatES.format(System.nanoTime() - startTime)));
		
		startTime = System.nanoTime();
		for(i=0; i<cantPruebas; i++)
			hashSet.remove("test"+i);
		System.out.println(String.format("Eliminar %d elementos tardo %s nanosegundos\n", cantPruebas, numberFormatES.format(System.nanoTime() - startTime)));
		
		System.out.println("--- ARRAYLIST ---");
		
		startTime = System.nanoTime();
		for(i=0; i<cantPruebas; i++)
			arrayList.add(0, "test"+i);
		System.out.println(String.format("Agregar %d elementos tardo %s nanosegundos", cantPruebas, numberFormatES.format(System.nanoTime() - startTime)));
		
		startTime = System.nanoTime();
		for(i=0; i<cantPruebas; i++)
			arrayList.remove(0);
		System.out.println(String.format("Eliminar %d elementos tardo %s nanosegundos\n", cantPruebas, numberFormatES.format(System.nanoTime() - startTime)));
		
		System.out.println("--- LINKEDLIST ---");
		
		startTime = System.nanoTime();
		for(i=0; i<cantPruebas; i++)
			linkedList.add(0, "test"+i);
		System.out.println(String.format("Agregar %d elementos tardo %s nanosegundos", cantPruebas, numberFormatES.format(System.nanoTime() - startTime)));
		
		startTime = System.nanoTime();
		for(i=0; i<cantPruebas; i++){
			linkedList.remove(0);
		}
		System.out.println(String.format("Eliminar %d elementos tardo %s nanosegundos\n", cantPruebas, numberFormatES.format(System.nanoTime() - startTime)));
		
		
		System.out.println("--- HASHMAP ---");
		
		startTime = System.nanoTime();
		for(i=0; i<cantPruebas; i++)
			hashMap.put("test"+i, testStr);
		System.out.println(String.format("Agregar %d elementos tardo %s nanosegundos", cantPruebas, numberFormatES.format(System.nanoTime() - startTime)));
		
		startTime = System.nanoTime();
		for(i=0; i<cantPruebas; i++){
			hashMap.remove("test"+i);
		}
		System.out.println(String.format("Eliminar %d elementos tardo %s nanosegundos\n", cantPruebas, numberFormatES.format(System.nanoTime() - startTime)));
	}
}
