package py.edu.uca.javase.ejercicios;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import com.lambdaworks.crypto.SCryptUtil;

import net.miginfocom.swing.MigLayout;

public class Clase11EJ1 {

	static String username = "victor";
	static String scryptedPass = "$s0$c1010$QKkpoazADz6UFXIEaus6+w==$dtNg+D0vGms7Y1ghhVkUHShzmDLfK83yq2ZSW6q78Wo=";
	
	public static void main(String[] args) {
		//creamos un JFrame
		JFrame frame = new JFrame(); 
		frame.getContentPane().setLayout(new MigLayout()); //establecemos el layout
		frame.setSize(new Dimension(330, 130)); //dimension ancho x alto
		frame.setLocation(100, 100); //en que posicion de la pantalla deseamos que aparezca
		frame.setResizable(true); //definimos que se pueda o no redimensionar el frame
		frame.setTitle("Login - SCrypt"); //texto que aparece en la barra superior
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE); //eliminar la ventana al cerrar
		
		//agregamos el label
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setVisible(true);
		
		//agregamos el textfield
		final JTextField txtUsername = new JTextField("");
		txtUsername.setVisible(true);
		
		//agregamos el label
		JLabel lblPass = new JLabel("Password:");
		lblPass.setVisible(true);
		
		//agregamos el textfield
		final JTextField txtPassword = new JTextField("");
		txtPassword.setVisible(true);
		
		//agregamos el textfield
		JButton btnLogin = new JButton("Iniciar sesión");
		btnLogin.setVisible(true);
		
		//agregamos al panel con las propiedades del alyout
		frame.add(lblUsername, "w 100:150:200");
		frame.add(txtUsername, "w 100:150:200, wrap");
		frame.add(lblPass, "w 100:150:200");
		frame.add(txtPassword, "w 100:150:200, wrap");
		frame.add(btnLogin, "split 2, spanx, grow, wrap");
		
		btnLogin.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				if(txtPassword.getText().equals("")){
		        	JOptionPane.showMessageDialog(null, "Favor ingrese un password.", "Atencion", JOptionPane.WARNING_MESSAGE);
		        	return;
				}
				
		        boolean matched = SCryptUtil.check(txtPassword.getText(), scryptedPass);
		        if(matched && txtUsername.getText().equals(username)){
		        	JOptionPane.showMessageDialog(null, "CORRECTO.", "OK", JOptionPane.INFORMATION_MESSAGE);
		        }else{
		        	JOptionPane.showMessageDialog(null, "INCORRECTO.", "ERROR", JOptionPane.ERROR_MESSAGE);
		        }				
			}
		});
		
		frame.setVisible(true); //mostramos el frame
	}
}
