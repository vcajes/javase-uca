package py.edu.uca.javase.ejercicios;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.WindowConstants;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import net.miginfocom.swing.MigLayout;
import py.edu.uca.javase.clase07.MyTableModel;

public class Clase7EJ1 {

	public static LinkedList<Persona> listaPersonas = new LinkedList<Persona>();
	public static JLabel labelNombre = new JLabel("Nombre");
	public static JTextField txtNombre = new JTextField();
	public static JLabel labelApellido = new JLabel("Apellido");
	public static JTextField txtApellido = new JTextField();
	public static JLabel labelSexo = new JLabel("Sexo");
	public static JComboBox<String> cbSexo = new JComboBox<String>();
	public static JCheckBox chkReligion = new JCheckBox("Religión Católica?");
	public static JButton botonAgregar = new JButton("Agregar Persona");
	public static JButton botonEditar = new JButton("Editar Persona");
	public static JButton botonBorrar = new JButton("Borrar Persona");
	public static JFrame frame = new JFrame();
	
	
	public static MyTableModel modeloTabla = new MyTableModel(); //creamos un modelo de tabla generico
	public static JTable tabla = new JTable(modeloTabla); //creamos la tabla con nuestro modelo de tabla
	public static JScrollPane scrollPane = new JScrollPane(tabla); //creamos un panel con scroll y ponemos adentro la tabla
	
	public static int idPersonaSel = -1;
	
	class Persona{
		public int id;
		public String nombre;
		public String apellido;
		public String sexo;
		public boolean esCatolico;
		public Persona(int id, String nombre, String apellido, String sexo, boolean catolico){
			this.id = id;
			this.nombre = nombre;
			this.apellido = apellido;
			this.sexo = sexo;
			this.esCatolico = catolico;
		}
	}
	
	public static void main(String[] args) {
		final Clase7EJ1 clase7 = new Clase7EJ1();
		
		//creamos un JFrame
		frame.getContentPane().setLayout(new MigLayout());
		frame.setSize(new Dimension(500, 500));
		frame.setLocation(100, 100);
		frame.setResizable(true);
		frame.setTitle("Clase 7 - Ejercicio 1");
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		
		botonEditar.setEnabled(false);
		botonBorrar.setEnabled(false);
		
		//cargamos los elementos al combo
		cbSexo.addItem("Masculino");
		cbSexo.addItem("Femenino");
		cbSexo.setSelectedIndex(0);

		modeloTabla.addColumn("Nº"); //agregamos la 1ra columna
		modeloTabla.addColumn("Nombre"); //agregamos la 2da columna
		modeloTabla.addColumn("Apellido"); //agregamos la 3ra columna
		modeloTabla.addColumn("Sexo"); //agregamos la 4ta columna
		modeloTabla.addColumn("Catolico"); //agregamos la 5ta columna
		
		//capturamos los eventos
		botonAgregar.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				Persona persona = clase7.new Persona(
					listaPersonas.size() + 1,
					txtNombre.getText(),
					txtApellido.getText(),
					(String)cbSexo.getSelectedItem(),
					chkReligion.isSelected()
				);
				listaPersonas.add(persona);
				mostrarPersonasEnTabla();
				JOptionPane.showMessageDialog(null, "Se agrego correctamente la persona.",
						"OK", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		botonBorrar.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				for(Persona p: listaPersonas)
					if(p.id == idPersonaSel){
						listaPersonas.remove(p);
						break;
					}
				
				mostrarPersonasEnTabla();
				JOptionPane.showMessageDialog(null, "Se borro correctamente la persona.",
						"OK", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		botonEditar.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				//falta implementar
			}
		});
		
		tabla.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				int fila = tabla.rowAtPoint(e.getPoint());
				
				//copiamos los valores a la interfaz
				txtNombre.setText((String)tabla.getValueAt(fila,1));
				txtApellido.setText((String)tabla.getValueAt(fila,2));
				cbSexo.setSelectedItem((String)tabla.getValueAt(fila,3));
				chkReligion.setSelected((boolean)tabla.getValueAt(fila,4));
				
				//guardamos el ID de la persona seleccionada
				idPersonaSel = (int) tabla.getValueAt(fila,0);
				
				//habilitamos los botones para editar y borrar
				botonEditar.setEnabled(true);
				botonBorrar.setEnabled(true);
			}});
		
		//agregamos el ordenador por columna
		RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(modeloTabla);
        tabla.setRowSorter(sorter);
		
        frame.add(labelNombre);
        frame.add(txtNombre, "gapleft 15px, spanx, growx, wrap");
        frame.add(labelApellido);
        frame.add(txtApellido, "gapleft 15px, w 200:5000:5000, growx, wrap");
        frame.add(labelSexo);
        frame.add(cbSexo, "gapleft 15px, w 200:5000:5000, growx, wrap");
        frame.add(chkReligion, "spanx, growx, wrap");
        frame.add(botonAgregar, "spanx, split 3, growx");
        frame.add(botonEditar, "growx");
        frame.add(botonBorrar, "growx, wrap");
		frame.add(scrollPane, "width 100:5000:5000, height 100:5000:5000, grow, span, wrap");
		frame.setVisible(true); //mostramos el frame
	}
	
	public static void mostrarPersonasEnTabla(){
		//eliminamos todo lo que hay en la tabla
		botonEditar.setEnabled(false);
		botonBorrar.setEnabled(false);
		
		while(modeloTabla.getRowCount() > 0)
			modeloTabla.removeRow(0);
		
		for(Persona p : listaPersonas)
			modeloTabla.addRow(new Object[]{
				p.id,
				p.nombre,
				p.apellido,
				p.sexo,
				p.esCatolico
			});
	}
}
