package py.edu.uca.javase.ejercicios;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Clase5EJ2 implements Serializable{

	private static final long serialVersionUID = 1L;

	public static void main(String[] args) {
		Clase5EJ2 clase5 = new Clase5EJ2();
		Auto[] autos = new Auto[3];
		Camioneta[] camionetas = new Camioneta[3];
		Moto[] motos = new Moto[3];
		
		//creamos las instancias de los objetos
		for(int i=0; i<3; i++){
			autos[i] = clase5.new Auto("NombreAuto"+i, "MarcaAuto"+i, 2000+i);
			camionetas[i] = clase5.new Camioneta("NombreCamioneta"+i, "MarcaCamioneta"+i, 2000+i);
			motos[i] = clase5.new Moto("NombreMoto"+i, "MarcaMoto"+i, 2000+i);
		}
		
		//serializamos a un archivo del disco duro
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("datosClaseEJ2.dat", false));
			for(int i=0; i<3; i++)
				oos.writeObject(autos[i]);
			for(int i=0; i<3; i++)
				oos.writeObject(camionetas[i]);
			for(int i=0; i<3; i++)
				oos.writeObject(motos[i]);			
			oos.flush();
			oos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//deserializamos el archivo a instancias de clases Java
		ObjectInputStream oos = null;
		try {
			oos = new ObjectInputStream(new FileInputStream("datosClaseEJ2.dat"));
			for(int i=0; i<3; i++)
				System.out.println((Auto) oos.readObject());
			for(int i=0; i<3; i++)
				System.out.println((Camioneta) oos.readObject());
			for(int i=0; i<3; i++)
				System.out.println((Moto) oos.readObject());
		} catch (ClassNotFoundException e2){
			e2.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} finally {
			try {
				oos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	class Auto implements Serializable{
		private static final long serialVersionUID = 1L;
		public String nombre;
		public String marca;
		public int año;
		public Auto(String nombre, String marca, int año){ this.nombre = nombre; this.marca = marca; this.año = año; }
		@Override public String toString(){ return String.format("AUTO | Nombre: %s | Marca: %s | Año: %d", nombre, marca, año); }
	}

	class Camioneta implements Serializable{
		private static final long serialVersionUID = 1L;
		public String nombre;
		public String marca;
		public int año;
		public Camioneta(String nombre, String marca, int año){ this.nombre = nombre; this.marca = marca; this.año = año; }
		@Override public String toString(){ return String.format("CAMIONETA | Nombre: %s | Marca: %s | Año: %d", nombre, marca, año); }
	}
	
	class Moto implements Serializable{
		private static final long serialVersionUID = 1L;
		public String nombre;
		public String marca;
		public int año;
		public Moto(String nombre, String marca, int año){ this.nombre = nombre; this.marca = marca; this.año = año; }
		@Override public String toString(){ return String.format("MOTO | Nombre: %s | Marca: %s | Año: %d", nombre, marca, año); }
	}
}
