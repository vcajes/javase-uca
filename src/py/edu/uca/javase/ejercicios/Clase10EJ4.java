package py.edu.uca.javase.ejercicios;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Clase10EJ4 {

	private static Logger log = LogManager.getLogger(Clase10EJ4.class);
	
	public static void main(String[] args){
		Connection conn = null; Statement stmt = null;	    
	    try{
		    Class.forName("org.sqlite.JDBC");
		    conn = DriverManager.getConnection("jdbc:sqlite::memory:");
		    System.out.println("Conexión a la DB correcta.");
		
		    stmt = conn.createStatement();
		    String sql = "CREATE TABLE PERSONAS (ID INT PRIMARY KEY NOT NULL," +
		                 "NOMBRE TEXT NOT NULL,  APELLIDO TEXT NOT NULL," + 
		                 "EDAD INT NOT NULL, FECHANAC DATE  NOT NULL, SEXO CHAR(1) NOT NULL)"; 
		    stmt.executeUpdate(sql);		    
		    System.out.println("La tabla PERSONAS se ha creado correctamente.");
		    
		    sql = "INSERT INTO PERSONAS (ID,NOMBRE,APELLIDO,EDAD,FECHANAC,SEXO) "+
		    	  "VALUES (?,?,?,?,?,?);"; 
		    PreparedStatement pstmt = conn.prepareStatement(sql);
		    pstmt.setInt(1, 1);
		    pstmt.setString(2, "Juan");
		    pstmt.setString(3, "Perez");
		    pstmt.setInt(4, 25);
		    pstmt.setDate(5, new java.sql.Date((new Date()).getTime()));
		    pstmt.setString(6, "M");
		    pstmt.executeUpdate();
		    stmt.close();
		    
		    sql = "INSERT INTO PERSONAS (ID,NOMBRE,APELLIDO,EDAD,FECHANAC,SEXO) "+
			    	  "VALUES (?,?,?,?,?,?);"; 
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, 3);
			pstmt.setString(2, "Laura");
			pstmt.setString(3, "Gimenez");
			pstmt.setInt(4, 35);
			pstmt.setDate(5, new java.sql.Date((new Date()).getTime()));
			pstmt.setString(6, "F");
			pstmt.executeUpdate();
			stmt.close();
		    
		    stmt = conn.createStatement();
		    ResultSet rs = stmt.executeQuery( "SELECT * FROM PERSONAS WHERE nombre LIKE '%au%';" );
		    while(rs.next()){
		    	log.info("ID = " + rs.getInt("ID") + " NOMBRE = " + rs.getString("NOMBRE")
		    			 + " APELLIDO = " + rs.getString("APELLIDO")
		    			 + " FECHA NACIMIENTO = " + rs.getDate("FECHANAC")
		    			+ " EDAD = " + rs.getInt("EDAD") +
		    			" SEXO = " + rs.getString("SEXO"));
		    }
		    rs.close(); stmt.close(); conn.close();
	    }catch(Exception e){
		    System.out.println(e.getClass().getName() + ": " + e.getMessage());
	    }    
	}
}
