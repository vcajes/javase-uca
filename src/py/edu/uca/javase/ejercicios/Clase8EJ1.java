package py.edu.uca.javase.ejercicios;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Scanner;

public class Clase8EJ1 {
	
	public static HashMap<String, PrintWriter> mapOutputs = new HashMap<String, PrintWriter>();

	public static void main(String[] args) {
		Clase8EJ1 echoserv = new Clase8EJ1();
		ServerSocket servidor = null;
		try{
			//creamos un servidor que escuchara en el puerto 9870
			servidor = new ServerSocket(9870);
			
			while(true){
				Socket socketEntrada = servidor.accept();
				System.out.println("Se conecto el cliente: " + socketEntrada.getRemoteSocketAddress());
				(new Thread(echoserv.new ManejadorEcho(socketEntrada))).start();
			}
		}catch(IOException ioex){
			ioex.printStackTrace();
		}finally{
			try {
				servidor.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	class ManejadorEcho implements Runnable{
		private Socket socket;
		private String username;
		public ManejadorEcho(Socket socket){ this.socket = socket; username = null; }
		
		@Override
		public void run() {
			try{
				InputStream inStream = socket.getInputStream();
				OutputStream outStream = socket.getOutputStream();
				Scanner in = new Scanner(inStream);
				PrintWriter out = new PrintWriter(outStream, true);
				out.println("Hola! Ingrese SALIR para terminar el servidor");
				
				boolean terminar = false;
				while(!terminar && in.hasNextLine()){
					String line = in.nextLine();
					
					if(line.trim().equals("SALIR"))
						terminar = true;
					
					try{
						if(line.startsWith("$username ")){
							username = line.split(" ")[1];
							synchronized(mapOutputs){
								mapOutputs.put(username, out);
							}
							out.println("Bienvenido " + username);
						}else{
							if(username == null){
								out.println("Primero debe registrarte para poder utilizar el chat.");
							}else{
								String[] values = line.split(" ", 2);
								if(values.length == 2){
									synchronized(mapOutputs){
										if(mapOutputs.containsKey(values[0])){ //verificamos que el usuario destino existe
											try{
												mapOutputs.get(values[0]).println(username + " dice:"); //enviamos al usuario destino el mensaje
												mapOutputs.get(values[0]).println("\t"+values[1]); //enviamos al usuario destino el mensaje
											}catch(Exception ex1){
												out.println("No se pudo enviar el mensaje el usuario: " + values[0]);
											}
										}else{
											out.println("El destino del mensaje no existe"); //avisamos al emisor del mensaje que el destino no existe
										}
									}
								}
							}
						}
					}catch(Exception ex){
						
					}
				}
				in.close();	
			}catch(IOException ioex){
				ioex.printStackTrace();
			}finally{
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				System.out.println("Se cerro la conexión con el cliente: " + socket.getRemoteSocketAddress());
			}
		}		
	}
}
