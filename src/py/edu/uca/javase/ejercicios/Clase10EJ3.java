package py.edu.uca.javase.ejercicios;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Scanner;

public class Clase10EJ3 {
	
	public static HashMap<String, PrintWriter> mapOutputs = new HashMap<String, PrintWriter>();

	public static void main(String[] args) {
		ServerSocket servidor = null;
		try{
			//creamos un servidor que escuchara en el puerto 9870
			servidor = new ServerSocket(9870);
			
			while(true){
				Socket socketEntrada = servidor.accept();
				System.out.println("Se conecto el cliente: " + socketEntrada.getRemoteSocketAddress());
				InputStream inStream = socketEntrada.getInputStream();
				Scanner in = new Scanner(inStream);
				System.out.println(in.nextLine());
				in.close();
			}
		}catch(IOException ioex){
			ioex.printStackTrace();
		}finally{
			try {
				servidor.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
