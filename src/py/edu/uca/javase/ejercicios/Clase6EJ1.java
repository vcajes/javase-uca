package py.edu.uca.javase.ejercicios;

public class Clase6EJ1 {

	public static void main(String[] args) {
		Clase6EJ1 clase6 = new Clase6EJ1();
		BancoProblematico banco = clase6.new BancoProblematico(5, 100000.0);
		//BancoCorrecto banco = clase6.new BancoCorrecto(5, 100000.0);
		for(int i=0; i<5; i++){
			TransferenciaRunnable tr = clase6.new TransferenciaRunnable(banco, i, 100000.0);
			Thread thread = new Thread(tr);
			thread.start();
		}
	}
	
	class TransferenciaRunnable implements Runnable{
		private BancoProblematico bancoProb;
		private BancoCorrecto bancoCorrecto;
		private int cuentaOrigen;
		private double montoMaximoTransf;
		
		public TransferenciaRunnable(BancoProblematico banco, int origen, double montoMaximoTransf){
			this.bancoProb = banco;
			this.cuentaOrigen = origen;
			this.montoMaximoTransf = montoMaximoTransf;
			this.bancoCorrecto = null;
		}
		
		public TransferenciaRunnable(BancoCorrecto banco, int origen, double montoMaximoTransf){
			this.bancoCorrecto = banco;
			this.cuentaOrigen = origen;
			this.montoMaximoTransf = montoMaximoTransf;
			this.bancoProb = null;
		}
		
		@Override
		public void run(){
			try{
				while(true){
					if(bancoCorrecto != null){
						int cuentaDestino = (int) (bancoCorrecto.getCantidadCuentas() * Math.random());
						double monto = montoMaximoTransf * Math.random();
						bancoCorrecto.transferirDinero(cuentaOrigen, cuentaDestino, monto);
						Thread.sleep((int) (5 * Math.random()));	
					}else if(bancoProb != null){
						int cuentaDestino = (int) (bancoProb.getCantidadCuentas() * Math.random());
						double monto = montoMaximoTransf * Math.random();
						bancoProb.transferirDinero(cuentaOrigen, cuentaDestino, monto);
						Thread.sleep((int) (5 * Math.random()));	
					}
				}
			}catch(InterruptedException ie){ }
		}
	}

	class BancoProblematico{
		private final double[] cuentas;
		
		public BancoProblematico(int cantidadCuentas, double balanceInicial){
			cuentas = new double[cantidadCuentas];
			for(int i=0; i<cuentas.length; i++)
				cuentas[i] = balanceInicial;
		}
		
		public void transferirDinero(int origen, int destino, double monto){
			if(cuentas[origen] < monto)
				return;
			//System.out.println(Thread.currentThread());
			cuentas[origen] -= monto;
			System.out.printf("Se transfirio %10.2f desde la cuenta %d a la cuenta %d\n", monto, origen, destino);
			cuentas[destino] += monto;
			System.out.printf("Balance total %10.2f\n", getBalanceTotal());
		}
		
		public double getBalanceTotal(){
			double suma = 0;
			for(double saldo : cuentas)
				suma += saldo;
			return suma;
		}
		
		public int getCantidadCuentas(){
			return this.cuentas.length;
		}
	}
	
	class BancoCorrecto{
		private final double[] cuentas;
		
		public BancoCorrecto(int cantidadCuentas, double balanceInicial){
			cuentas = new double[cantidadCuentas];
			for(int i=0; i<cuentas.length; i++)
				cuentas[i] = balanceInicial;
		}
		
		public synchronized void transferirDinero(int origen, int destino, double monto){
			try{
				while(cuentas[origen] < monto)
					wait(); //dormimos al hilo hasta que se haga una transferencia correcta
				
				//System.out.println(Thread.currentThread());
				cuentas[origen] -= monto;
				System.out.printf("Se transfirio %10.2f desde la cuenta %d a la cuenta %d\n", monto, origen, destino);
				cuentas[destino] += monto;
				System.out.printf("Balance total %10.2f\n", getBalanceTotal());
				notifyAll(); //despertamos a todos los hilos que puede que no tenian dinero en la cuenta de origen como para transferir
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		public synchronized double getBalanceTotal(){
			double suma = 0;
			for(double saldo : cuentas)
				suma += saldo;
			return suma;
		}
		
		public synchronized int getCantidadCuentas(){
			return this.cuentas.length;
		}
	}
}
