package py.edu.uca.javase.ejercicios;

public class Clase3EJ1 {

	public static void main(String[] args){
		Clase3EJ1 padre = new Clase3EJ1();
		FiguraGeometrica f1 = padre.new FiguraGeometrica(0, 10);
		FiguraGeometrica f2 = padre.new FiguraGeometrica(0, 15);
		System.out.println(f1);
		System.out.println(f1.equals(f2) == true ? "f1 y f2 son iguales" : "esto nunca se va imprimir porque se lanza excepcion");
		
		Cilindro c1 = padre.new Cilindro(0, 10, 5.3, 8.9);
		Cilindro c2 = padre.new Cilindro(0, 10, 5.3, 8.9);
		System.out.println(c1);
		System.out.println(c2);
		System.out.println(c1.equals(c2) == true ? "c1 y c2 son iguales" : "esto nunca se va imprimir porque se lanza excepcion");
		
		c2.radio = 90.7;
		//try{
			System.out.println(c1.equals(c2) == true ? "c1 y c2 son iguales" : "esto nunca se va imprimir porque se lanza excepcion");
		/*}catch(FigurasDistintasException fde){
			System.out.println("Aca capturamos la excepcion personalizada de FigurasDistintasException");
			//imprimimos el error de nuestra excepcion
			System.out.println(fde.getMessage());
			//imprimimos la pila de llamadas del error java
			fde.printStackTrace();
		}*/
		
		System.out.println("El programa continua aqui, no termina puesto que capturamos la excepcion.");
	}
	
	@SuppressWarnings("serial")
	public class FigurasDistintasException extends RuntimeException{
		private String message;
		
		public FigurasDistintasException(String message){
			super(message);
			this.message = message;
		}
		
		public String getMessage(){ return this.message; }
	}
	
	public class FiguraGeometrica{
		protected String nombre;
		protected int posX;
		protected int posY;
		
		public FiguraGeometrica(int posX, int posY){
			this.nombre = "Figura Geometrica";
			this.posX = posX;
			this.posY = posY;
		}
		
		public FiguraGeometrica(String nombre, int posX, int posY){
			this.nombre = nombre;
			this.posX = posX;
			this.posY = posY;
		}
		
		@Override
		public boolean equals(Object obj) throws FigurasDistintasException{
			if(obj instanceof FiguraGeometrica){
				if(((FiguraGeometrica) obj).nombre.equals(this.nombre)){
					return true;
				}
			}
			throw new FigurasDistintasException("Las Figuras Geometricas no son iguales.");
		}
		
		@Override
		public String toString(){
			return String.format("%s", this.nombre);
		}
	}
	
	public class Circulo extends FiguraGeometrica{
		protected double radio;
		
		public Circulo(double radio){
			super("Circulo", 0, 0);
			this.radio = radio;
		}
		
		public Circulo(String nombre, int posX, int posY, double radio){
			super(nombre, posX, posY);
			this.radio = radio;
		}
		
		@Override
		public boolean equals(Object obj) throws FigurasDistintasException{
			if(obj instanceof Circulo){
				if(((Circulo) obj).radio == this.radio && ((Circulo) obj).nombre.equals(this.nombre)){
					return true;
				}
			}
			throw new FigurasDistintasException("Los circulos no son iguales.");
		}
		
		@Override
		public String toString(){
			return String.format("%s | Radio %.2f", this.nombre, this.radio);
		}
	}
	
	public class Cilindro extends Circulo{
		protected double altura;
		
		public Cilindro(double altura){
			super("Cilindro", 0, 0, altura);
			this.altura = altura;
		}
		
		public Cilindro(int posX, int posY, double radio, double altura){
			super("Cilindro", 0, 0, radio);
			this.altura = altura;
		}
		
		@Override
		public boolean equals(Object obj) throws FigurasDistintasException{
			if(obj instanceof Cilindro){
				if(((Cilindro) obj).radio == this.radio && ((Cilindro) obj).altura == this.altura  && ((Cilindro) obj).nombre.equals(this.nombre)){
					return true;
				}
			}
			throw new FigurasDistintasException("Los cilindros no son iguales.");
		} 
		
		@Override
		public String toString(){
			return String.format("%s | Radio %.2f | Altura %.2f", this.nombre, this.radio, this.altura);
		}
	}	
}
