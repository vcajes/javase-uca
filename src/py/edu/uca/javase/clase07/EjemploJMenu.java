package py.edu.uca.javase.clase07;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.WindowConstants;

import net.miginfocom.swing.MigLayout;

public class EjemploJMenu {

	public static void main(String[] args) {
		//creamos un JFrame
		JFrame frame = new JFrame(); 
		frame.getContentPane().setLayout(new MigLayout()); //establecemos el layout
		frame.setSize(new Dimension(400, 200)); //dimension ancho x alto
		frame.setLocation(100, 100); //en que posicion de la pantalla deseamos que aparezca
		frame.setResizable(true); //definimos que se pueda o no redimensionar el frame
		frame.setTitle("Ejemplo JMenu"); //texto que aparece en la barra superior
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE); //eliminar la ventana al cerrar

		JMenuBar menuBar = new JMenuBar(); //creamos el menu bar
		
		JMenu mInicio = new JMenu("Inicio"); //cramos el menu inicio
		JMenu mAyuda = new JMenu("Ayuda"); //creamos el menu ayuda
		
		//agregamos los menu items a los menus
	    JMenuItem mAbrir = new JMenuItem("Abrir");              mInicio.add(mAbrir);
	    JMenuItem mGuardar = new JMenuItem("Guardar como");     mInicio.add(mGuardar);
	    JMenuItem mSalir = new JMenuItem("Salir");              mAyuda.add(mSalir);
	    JMenuItem mAcercaDe = new JMenuItem("Acerca de.");      mAyuda.add(mAcercaDe);
	    
	    //agregamos una accion al hacer click en el menu salir
	    mSalir.addActionListener(new ActionListener(){@Override	public void actionPerformed(ActionEvent ae){
        	System.exit(0);
        }});
		
	    menuBar.add(mInicio); menuBar.add(mAyuda); //agregamos los menus al menu bar
	    frame.setJMenuBar(menuBar); //agregamos el menu bar al frame
	    frame.setVisible(true); //mostramos el frame
	}
}
