package py.edu.uca.javase.clase07;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

import net.miginfocom.swing.MigLayout;

public class EjemploJFrame {

	public static void main(String args[]){
		JFrame frame = new JFrame(); //creamos un JFrame
		
		//seteamos sus propiedades
		frame.getContentPane().setLayout(new MigLayout()); //establecemos el layout
		frame.setSize(new Dimension(400, 200)); //dimension ancho x alto
		frame.setLocation(100, 100); //en que posicion de la pantalla deseamos que aparezca
		frame.setResizable(true); //definimos que se pueda o no redimensionar el frame
		frame.setTitle("Titulo del frame"); //texto que aparece en la barra superior
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE); //eliminar la ventana al cerrar
		frame.setVisible(true); //para que se vea la ventana en la pantalla
	}
}
