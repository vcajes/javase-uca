package py.edu.uca.javase.clase07;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

import net.miginfocom.swing.MigLayout;

public class EjemploComboAndCheckBox {
	
	public static void main(String[] args) {
		//creamos un JFrame
		JFrame frame = new JFrame(); 
		frame.getContentPane().setLayout(new MigLayout()); //establecemos el layout
		frame.setSize(new Dimension(350, 120)); //dimension ancho x alto
		frame.setLocation(100, 100); //en que posicion de la pantalla deseamos que aparezca
		frame.setResizable(true); //definimos que se pueda o no redimensionar el frame
		frame.setTitle("Ejemplo ComboBox y ChechBox"); //texto que aparece en la barra superior
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE); //eliminar la ventana al cerrar
		
		//agregamos el label
		JLabel label = new JLabel("Label para el combo");
		label.setFont(new Font("Calibri", 1, 14)); //seteamos una fuente
		label.setVisible(true);
		
		//agregamos el textfield
		JComboBox<String> combo = new JComboBox<String>();
		combo.setFont(new Font("Calibri", 1, 14)); //seteamos una fuente
		combo.addItem("Item 1"); //agregamos un item para el combo
		combo.addItem("Pelota"); //agregamos un item para el combo
		combo.addItem("Tomate"); //agregamos un item para el combo
		combo.setVisible(true);
		
		combo.setSelectedItem("Tomate");//seteamos un valor del combo
		System.out.println(combo.getSelectedIndex() + " --> " + (String)combo.getSelectedItem()); //obtenemos el valor seleccionado
		
		JCheckBox check = new JCheckBox("Este es el label del checkbox"); //creamos el checkbox
		check.setSelected(true); //lo preseleccionamos
		System.out.println(check.isSelected()); //esta o no seleccionado?
		
		//agregamos al panel con las propiedades del alyout
		frame.add(label, "w 100:150:200, grow");
		frame.add(combo, "w 100:150:200, gapleft 15, wrap");
		frame.add(check, "w 300!, spanx");
		
		frame.setVisible(true); //mostramos el frame
	}
}
