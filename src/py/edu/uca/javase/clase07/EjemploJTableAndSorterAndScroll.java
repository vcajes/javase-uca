package py.edu.uca.javase.clase07;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.WindowConstants;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import net.miginfocom.swing.MigLayout;

public class EjemploJTableAndSorterAndScroll {

	public static void main(String[] args) {
		//creamos un JFrame
		JFrame frame = new JFrame(); 
		frame.getContentPane().setLayout(new MigLayout());
		frame.setSize(new Dimension(350, 190));
		frame.setLocation(100, 100);
		frame.setResizable(true);
		frame.setTitle("Ejemplo Tabla, Sorter y Scroll");
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		
		//creamos la tabla
		MyTableModel modeloTabla = new MyTableModel(); //creamos un modelo de tabla generico
		final JTable tabla = new JTable(modeloTabla); //creamos la tabla con nuestro modelo de tabla
		JScrollPane scrollPane = new JScrollPane(tabla); //creamos un panel con scroll y ponemos adentro la tabla
		
		modeloTabla.addColumn("Nº"); //agregamos la 1ra columna
		modeloTabla.addColumn("Nombre"); //agregamos la 2da columna
		modeloTabla.addColumn("Apellido"); //agregamos la 3ra columna
		
		//agregamos filas a la tabla
		modeloTabla.addRow(new Object[]{1, "Lucas", "Mendoza"}); modeloTabla.addRow(new Object[]{2, "Luisa", "Leoz"});
		modeloTabla.addRow(new Object[]{3, "Enrique", "Igague"}); modeloTabla.addRow(new Object[]{4, "Josue", "Perez"});
		modeloTabla.addRow(new Object[]{5, "Ricardo", "Ariosa"}); modeloTabla.addRow(new Object[]{5, "Marcos", "Nangana"});
		modeloTabla.addRow(new Object[]{1, "Victor", "Duarte"}); modeloTabla.addRow(new Object[]{2, "Luis", "Calcena"});
		modeloTabla.addRow(new Object[]{3, "Martin", "Caceres"}); modeloTabla.addRow(new Object[]{4, "Elias", "Davalos"});
				
		//capturamos el evento de cuando hacemos click en una fila
		tabla.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e){
				int fila = tabla.rowAtPoint(e.getPoint());
				System.out.println(String.format("Fila: %d", fila));
				System.out.println(String.format("El valor de la fila es: (%d,%s,%s)", 
						tabla.getValueAt(fila,0),tabla.getValueAt(fila,1),tabla.getValueAt(fila,2)));
		}});
		
		//agregamos el ordenador por columna
		RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(modeloTabla);
        tabla.setRowSorter(sorter);
		
		frame.add(scrollPane, "grow, span, wrap");
		frame.setVisible(true); //mostramos el frame
	}

}
