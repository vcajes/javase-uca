package py.edu.uca.javase.clase07;

import javax.swing.table.DefaultTableModel;

public class MyTableModel extends DefaultTableModel{
	private static final long	serialVersionUID	= 1L;

	@Override
	public boolean isCellEditable(int row, int column){
		return false;
	}
	
	@Override
	@SuppressWarnings({"unchecked", "rawtypes"})
	public Class getColumnClass(int column){
        if ((column >= 0) && (column < getColumnCount()) && getRowCount() > 0)
        	if(getValueAt(0, column) != null)
        		if(getValueAt(0, column).getClass() != null)
        			return getValueAt(0, column).getClass();
    	return Object.class;
    }
}
