package py.edu.uca.javase.clase07;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import net.miginfocom.swing.MigLayout;

public class EjemploJButtonAndEvents {

	public static void main(String[] args) {
		//creamos un JFrame
		JFrame frame = new JFrame(); 
		frame.getContentPane().setLayout(new MigLayout()); //establecemos el layout
		frame.setSize(new Dimension(400, 200)); //dimension ancho x alto
		frame.setLocation(100, 100); //en que posicion de la pantalla deseamos que aparezca
		frame.setResizable(true); //definimos que se pueda o no redimensionar el frame
		frame.setTitle("Ejemplo Boton y Eventos"); //texto que aparece en la barra superior
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE); //eliminar la ventana al cerrar
		
		//agregamos el label
		JLabel label = new JLabel("label text");
		label.setFont(new Font("Calibri", 1, 14)); //seteamos una fuente
		label.setVisible(true);
		
		//agregamos el textfield
		final JTextField textField = new JTextField("texto inicial");
		textField.setFont(new Font("Calibri", 1, 14)); //seteamos una fuente
		textField.setVisible(true);
		
		//creamos un boton
		JButton boton = new JButton("click Me!");
		
		//capturamos el evento click del boton
		boton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				JOptionPane.showMessageDialog(null, "El JTextField tiene como valor: " + textField.getText(),
						"Titulo", JOptionPane.INFORMATION_MESSAGE);
				}
		});
		
		//capturamos las teclas presionadas por el en el TextField
		textField.addKeyListener(new KeyListener(){
			@Override  public void keyReleased(KeyEvent e){
				System.out.println("Soltaste la tecla: " + e.getKeyChar());
			}
			@Override public void keyPressed(KeyEvent e){
				//System.out.println("Presionaste y Soltaste la tecla: " + e.getKeyChar());
			}
			@Override	public void keyTyped(KeyEvent e){
				System.out.println("Apretaste la tecla: " + e.getKeyChar());
			}
		});
		
		//agregamos al panel con las propiedades del alyout
		frame.add(label, "w 100:150:200, grow, wrap");
		frame.add(textField, "w 100:150:200, wrap");
		frame.add(boton, "grow, span, wrap");
		
		frame.setVisible(true); //mostramos el frame
	}
}
