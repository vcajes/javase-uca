package py.edu.uca.javase.clase07;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

import net.miginfocom.swing.MigLayout;

public class EjemploTextField {

	public static void main(String[] args) {
		//creamos un JFrame
		JFrame frame = new JFrame(); 
		frame.getContentPane().setLayout(new MigLayout()); //establecemos el layout
		frame.setSize(new Dimension(400, 200)); //dimension ancho x alto
		frame.setLocation(100, 100); //en que posicion de la pantalla deseamos que aparezca
		frame.setResizable(true); //definimos que se pueda o no redimensionar el frame
		frame.setTitle("Ejemplo Label y TextField"); //texto que aparece en la barra superior
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE); //eliminar la ventana al cerrar
		
		//agregamos el label
		JLabel label = new JLabel("label text");
		label.setFont(new Font("Calibri", 1, 14)); //seteamos una fuente
		label.setVisible(true);
		
		//agregamos el textfield
		JTextField textField = new JTextField("texto inicial");
		textField.setFont(new Font("Calibri", 1, 14)); //seteamos una fuente
		textField.setVisible(true);
		
		//para obtener y setear algun valor nuevo
		System.out.println(textField.getText()); //obtenemos el texto del field
		textField.setText("algo nuevo para poner"); //seteamos un valor nuevo
		
		//agregamos al panel con las propiedades del alyout
		frame.add(label, "w 100:150:200, grow, wrap");
		frame.add(textField, "w 100:150:200, wrap");
		
		frame.setVisible(true); //mostramos el frame
	}

}
