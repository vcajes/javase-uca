package py.edu.uca.javase.clase10;

import org.apache.logging.log4j.*;

public class EjemploLog4J {

	private static Logger log = LogManager.getLogger(EjemploLog4J.class);
	
	public static void main(String[] args) {
		log.trace("Trace Message!");
		log.debug("Debug Message!");
		log.info("Info Message!");
		log.warn("Warn Message!");
		log.error("Error Message!");
		log.fatal("Fatal Message!");
		
		try {
			int b = 5 / 0;
		} catch(Exception e){
			log.error("Loggeamos una excepcion", e);
		}
	}
}


