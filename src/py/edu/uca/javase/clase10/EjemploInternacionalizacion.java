package py.edu.uca.javase.clase10;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class EjemploInternacionalizacion {

	public static void main(String[] args) {
		Locale localeEN = new Locale("en", "US"); //ingles de estados unidos
		Locale localeES = new Locale("es"); //español en general
		
		//va a buscar string.properties el paquete i18n de nuestro proyecto
		ResourceBundle stringsEnglish = ResourceBundle.getBundle("i18n.strings", localeEN);
		ResourceBundle stringsSpanish = ResourceBundle.getBundle("i18n.strings", localeES);
		System.out.println("En inglés de EEUU: " + stringsEnglish.getString("texto1"));
		System.out.println("En español: " + stringsSpanish.getString("texto1"));
		
		//Formatos de numeros
		NumberFormat numberFormatEN = NumberFormat.getInstance(localeEN);
		NumberFormat numberFormatES = NumberFormat.getInstance(localeES);		
		System.out.println("En inglés de EEUU: " + numberFormatEN.format(78.99)); //78.99
		System.out.println("En español: " + numberFormatES.format(78.99)); //78,99
		
		//Monedas
		NumberFormat currencyFormatEN = NumberFormat.getCurrencyInstance(localeEN);
		NumberFormat currencyFormatES = NumberFormat.getCurrencyInstance(localeES);		
		System.out.println("En inglés de EEUU: " + currencyFormatEN.format(78.99)); // $78.99
		System.out.println("En español: " + currencyFormatES.format(78.99)); // ¤78,99
		
		//Porcentajes
		NumberFormat percentageFormatEN = NumberFormat.getPercentInstance(localeEN);
		NumberFormat percentageFormatES = NumberFormat.getPercentInstance(localeES);		
		System.out.println("En inglés de EEUU: " + percentageFormatEN.format(100.0)); // 10,000%
		System.out.println("En español: " + percentageFormatES.format(100.0)); // 10.000%
		
		//Fechas
		DateFormat dateFormatEN = DateFormat.getDateInstance(DateFormat.DEFAULT, localeEN);
		DateFormat dateFormatES = DateFormat.getDateInstance(DateFormat.DEFAULT, localeES);
		System.out.println("En inglés de EEUU: " + dateFormatEN.format(new Date())); // Oct 21, 2014
		System.out.println("En español: " + dateFormatES.format(new Date())); // 21-oct-2014
		
		//Tiempo
		DateFormat timeFormatEN = DateFormat.getTimeInstance(DateFormat.DEFAULT, localeEN);
		DateFormat timeFormatES = DateFormat.getTimeInstance(DateFormat.DEFAULT, localeES);
		System.out.println("En inglés de EEUU: " + timeFormatEN.format(new Date())); // 8:38:45 AM
		System.out.println("En español: " + timeFormatES.format(new Date())); // 8:38:45
		
		//Fecha y Tiempo
		DateFormat datetimeFormatEN = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, localeEN);
		DateFormat datetimeFormatES = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, localeES);
		System.out.println("En inglés de EEUU: " + datetimeFormatEN.format(new Date())); // Oct 21, 2014 8:39:54 AM
		System.out.println("En español: " + datetimeFormatES.format(new Date())); // 21-oct-2014 8:39:54
		
		//Parseo de fechas
		String pattern = "dd/MM/yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		try {
			Date fecha = simpleDateFormat.parse("25/10/2013");
			System.out.println(fecha); //Fri Oct 25 00:00:00 PYST 2013
		} catch (ParseException e) {
			System.err.println("Error al parsear la fecha.");
			e.printStackTrace();
		}		
	}
}
