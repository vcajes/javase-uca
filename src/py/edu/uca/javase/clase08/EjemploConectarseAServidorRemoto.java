package py.edu.uca.javase.clase08;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class EjemploConectarseAServidorRemoto {

	public static void main(String[] args) {
		try {
			Socket socket = new Socket();
			
			//conectamos el socket al host remoto en el puerto 13, estableciendo un timeout de 5 segundos
			socket.connect(new InetSocketAddress("time-A.timefreq.bldrdoc.gov", 13), 5000);
			try{
				InputStream inStream = socket.getInputStream();
				Scanner in = new Scanner(inStream);
				while(in.hasNextLine()){
					String line = in.nextLine();
					System.out.println(line);
				}
				in.close();
			} finally {
				socket.close();
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
