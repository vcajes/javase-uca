package py.edu.uca.javase.clase08;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class ServidorEchoMultiplesClientes {

	public static void main(String[] args) {
		ServidorEchoMultiplesClientes echoserv = new ServidorEchoMultiplesClientes();
		ServerSocket servidor = null;
		try{
			//creamos un servidor que escuchara en el puerto 9870
			servidor = new ServerSocket(9870);
			
			while(true){
				Socket socketEntrada = servidor.accept();
				System.out.println("Se conecto el cliente: " + socketEntrada.getRemoteSocketAddress());
				(new Thread(echoserv.new ManejadorEcho(socketEntrada))).start();
			}
		}catch(IOException ioex){
			ioex.printStackTrace();
		}finally{
			try {
				servidor.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	class ManejadorEcho implements Runnable{
		private Socket socket;		
		public ManejadorEcho(Socket socket){ this.socket = socket; }
		
		@Override
		public void run() {
			try{
				InputStream inStream = socket.getInputStream();
				OutputStream outStream = socket.getOutputStream();
				Scanner in = new Scanner(inStream);
				PrintWriter out = new PrintWriter(outStream, true);
				out.println("Hola! Ingrese SALIR para terminar el servidor");
				
				boolean terminar = false;
				while(!terminar && in.hasNextLine()){
					String line = in.nextLine();
					System.out.println(line); //imprimimos localmente lo que recibimos
					out.println(line); //reenviamos al servidor remoto lo que recibimos
					if(line.trim().equals("SALIR"))
						terminar = true;
				}
				in.close();	
			}catch(IOException ioex){
				ioex.printStackTrace();
			}finally{
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				System.out.println("Se cerro la conexión con el cliente: " + socket.getRemoteSocketAddress());
			}
		}		
	}
}
