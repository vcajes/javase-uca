package py.edu.uca.javase.clase08;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class ServidorEchoSimple {

	public static void main(String[] args) {
		try{
			//creamos un servidor que escuchara en el puerto 9870
			ServerSocket servidor = new ServerSocket(9870);
			
			//bloqueamos el servidor hasta que recibimos una conexion de un cliente
			System.out.println("Ahora el servidor se bloquea y espera un cliente");
			Socket socketEntrada = servidor.accept();
			System.out.println("Se desbloquea el servidor, se conecto un cliente");
			
			try{
				InputStream inStream = socketEntrada.getInputStream();
				OutputStream outStream = socketEntrada.getOutputStream();
				Scanner in = new Scanner(inStream);
				PrintWriter out = new PrintWriter(outStream, true);
				out.println("Hola! Ingrese SALIR para terminar el servidor");
				
				boolean terminar = false;
				while(!terminar && in.hasNextLine()){
					String line = in.nextLine();
					System.out.println(line); //imprimimos localmente lo que recibimos
					out.println(line); //reenviamos al servidor remoto lo que recibimos
					if(line.trim().equals("SALIR"))
						terminar = true;
				}
				in.close();
			}finally{
				socketEntrada.close();
				servidor.close();
			}
		}catch(IOException ioex){
			ioex.printStackTrace();
		}
	}
}
