package py.edu.uca.javase.clase08;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class EjemploConexionesURL {

	public static void main(String[] args) {
		try {
			URL url = new URL("http://www.abc.com.py/");
			Scanner in = new Scanner(url.openStream());
			
			//imprimimos el HTML de la pagina Web de ABC
			while(in.hasNextLine()){
				String line = in.nextLine();
				System.out.println(line);
			}
			in.close();
			
			//propiedades de la conexion
			url = new URL("http://www.abc.com.py/");
			URLConnection conn = url.openConnection();
			conn.connect();
			System.out.println(conn.getContentType());
			System.out.println(conn.getContentLength());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
