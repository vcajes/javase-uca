package py.edu.uca.javase.clase08;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class EjemploInetAddress {

	public static void main(String[] args) {
		try {
			//obtenemos UNA IP de un servidor dado
			InetAddress address = InetAddress.getByName("time-A.timefreq.bldrdoc.gov");
			System.out.println(address.getCanonicalHostName() + " " + address.getHostAddress());
			
			//Obtenemos todas las IPs para un servidor dado
			for(InetAddress ia : InetAddress.getAllByName("abc.com.py"))
				System.out.println(ia.getHostAddress());
			
			//Obtenemos la IP de nuestra interfaz de red y el nombre de la maquina
			InetAddress localAddr = InetAddress.getLocalHost();
			System.out.println(localAddr.getCanonicalHostName() + " " + localAddr.getHostAddress());
		} catch (UnknownHostException e) {
			System.out.println("No se encontro el host solicitado.");
		}
	}
}
