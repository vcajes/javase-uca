package py.edu.uca.javase.clase09;
import java.sql.*;
public class EjemploSQLite{	
	public static void main(String args[]){
	    Connection conn = null; Statement stmt = null;	    
	    try{
		    Class.forName("org.sqlite.JDBC");
		    conn = DriverManager.getConnection("jdbc:sqlite::memory:");
		    System.out.println("Conexión a la DB correcta.");
		
		    stmt = conn.createStatement();
		    String sql = "CREATE TABLE EMPLEADOS (ID INT PRIMARY KEY NOT NULL, " +
		                 " NOMBRE  TEXT  NOT NULL, EDAD INT NOT NULL, SALARIO REAL)"; 
		    stmt.executeUpdate(sql);		    
		    System.out.println("La tabla EMPLEADOS se ha creado correctamente.");
		    
		    sql = "INSERT INTO EMPLEADOS (ID,NOMBRE,EDAD,SALARIO) VALUES (1, 'Juan', 20, 10000.00 );"; 
		    stmt.executeUpdate(sql); stmt.close();

		    sql = "INSERT INTO EMPLEADOS (ID,NOMBRE,EDAD,SALARIO) VALUES (2, 'Luis', 30, 20000.00 );"; 
		    stmt.executeUpdate(sql); stmt.close();

		    sql = "INSERT INTO EMPLEADOS (ID,NOMBRE,EDAD,SALARIO) VALUES (3, 'Pepe', 40, 30000.00 );"; 
		    stmt.executeUpdate(sql); stmt.close();		    
		    System.out.println("Se han insertado correctamente los registros.");
		    
		    stmt = conn.createStatement();
		    ResultSet rs = stmt.executeQuery( "SELECT * FROM EMPLEADOS;" );
		    while(rs.next()){
		        System.out.print("ID = " + rs.getInt("ID"));
		        System.out.print(" NOMBRE = " + rs.getString("NOMBRE"));
		        System.out.print(" EDAD = " + rs.getInt("EDAD"));
		        System.out.println(" SALARIO = " + rs.getFloat("SALARIO"));
		    }
		    rs.close(); stmt.close(); conn.close();
	    }catch(Exception e){
		    System.out.println(e.getClass().getName() + ": " + e.getMessage());
	    }    
	}
}