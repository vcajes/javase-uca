package py.edu.uca.javase.clase09;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

public class EjemploTransaccion {

	private static Connection conn = null;
	private static PreparedStatement preparedStatement = null;
	
	public static void main(String[] args) {
		try {
			//Verificamos si tenemos el driver JDBC
			Class.forName("com.mysql.jdbc.Driver");
			
			//Realizamos la conexion a la base de datos mysql
			conn = DriverManager.getConnection("jdbc:mysql://192.168.16.252:3306/javaseuca?"
					+ "user=root&password=CATTLE14050514&useUnicode=true&characterEncoding=utf-8");
			
			conn.setAutoCommit(false); //empieza el modo transaccion
			
			//Insertamos dos nuevos registros a la base de datos
			preparedStatement = conn.prepareStatement("INSERT INTO personas VALUES (default, ?, ?, ?, ?)");
			preparedStatement.setString(1, "julia");
			preparedStatement.setString(2, "rivarola");
			preparedStatement.setString(3, "F");
			preparedStatement.setDate(4, new java.sql.Date((new Date()).getTime()));
			preparedStatement.executeUpdate();
			preparedStatement.close();
			
			preparedStatement = conn.prepareStatement("INSERT INTO personas VALUES (default, ?, ?, ?, ?)");
			preparedStatement.setString(1, "juan");
			preparedStatement.setString(2, "fernandez");
			preparedStatement.setString(3, "M");
			preparedStatement.setDate(4, new java.sql.Date((new Date()).getTime()));
			preparedStatement.executeUpdate();
			preparedStatement.close();
			
			//si llegamos aca y no ocurrio ningun error, hacemos commit
			conn.commit(); //se envian los cambios realizados a la DB
			
			//Cerramos la conexion a la DB
			conn.close();
		} catch (Exception e) { //si ocurrio un error hacemos rollback
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
}
