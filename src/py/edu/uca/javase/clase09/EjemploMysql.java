package py.edu.uca.javase.clase09;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

public class EjemploMysql {

	private static Connection conn = null;
	private static Statement statement = null;
	private static PreparedStatement preparedStatement = null;
	private static ResultSet resultSet = null;
	
	public static void main(String[] args) {
		try {
			//Verificamos si tenemos el driver JDBC
			Class.forName("com.mysql.jdbc.Driver");
			
			//Realizamos la conexion a la base de datos mysql
			conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/javaseuca?"
					+ "user=root&password=pqntslc&useUnicode=true&characterEncoding=utf-8");
			
			//Consultamos la tabla 'personas' e imprimimos en la consola
			statement = conn.createStatement(); //permite realizar consultas a la db
			resultSet = statement.executeQuery("SELECT * FROM personas"); //consultamos
			while(resultSet.next()){ //iteramos fila por fila
				Integer id = resultSet.getInt("id");
				String nombre = resultSet.getString("nombre");
				String apellido = resultSet.getString("apellido");
				String sexo = resultSet.getString("sexo");
				Date nacimiento = resultSet.getDate("fecha_nacimiento");
				
				System.out.println(String.format("ID: %d, ID: %s, ID: %s, ID: %s, ID: %s, ", 
					id, nombre, apellido, sexo, nacimiento.toString()));
			}
			resultSet.close();
			statement.close();

			//Insertamos un nuevos registro a la base de datos
			preparedStatement = conn.prepareStatement("INSERT INTO personas VALUES (default, ?, ?, ?, ?)");
			preparedStatement.setString(1, "julia");
			preparedStatement.setString(2, "rivarola");
			preparedStatement.setString(3, "F");
			preparedStatement.setDate(4, new java.sql.Date((new Date()).getTime()));
			preparedStatement.executeUpdate();
			preparedStatement.close();
			
			//Modificamos un registro de la base de datos
			preparedStatement = conn.prepareStatement("UPDATE personas SET nombre = ?, apellido = ? WHERE ID = ?");
			preparedStatement.setString(1, "alicia");
			preparedStatement.setString(2, "britez");
			preparedStatement.setInt(3, 1);
			preparedStatement.executeUpdate();
			preparedStatement.close();
			
			//Consultamos la tabla personas utilizando parametros de filtro
			preparedStatement = conn.prepareStatement("SELECT * FROM personas WHERE id > ?");
			preparedStatement.setInt(1, 1);
			resultSet = preparedStatement.executeQuery();
			while(resultSet.next()){ //iteramos fila por fila
				Integer id = resultSet.getInt("id");
				String nombre = resultSet.getString("nombre");
				String apellido = resultSet.getString("apellido");
				String sexo = resultSet.getString("sexo");
				Date nacimiento = resultSet.getDate("fecha_nacimiento");
				
				System.out.println(String.format("ID: %d, ID: %s, ID: %s, ID: %s, ID: %s, ", 
					id, nombre, apellido, sexo, nacimiento.toString()));
			}
			resultSet.close();
			preparedStatement.close();
			
			//Eliminamos un registro de la DB
			preparedStatement = conn.prepareStatement("DELETE FROM personas WHERE nombre = ?");
			preparedStatement.setString(1, "julia");
			preparedStatement.executeUpdate();
			preparedStatement.close();
			
			//Cerramos la conexion a la DB
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
