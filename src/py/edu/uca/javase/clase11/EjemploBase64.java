package py.edu.uca.javase.clase11;

import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.binary.Base64;

public class EjemploBase64 {

	public static void main(String[] args) {
		//Base64 de un texto
    	String cadena1 = "texto a convertir a base64";
    	String cadena1Base64 = Base64.encodeBase64String(cadena1.getBytes());
    	System.out.println("El BASE64 de la cadena es: " + cadena1Base64);
    	try {
			System.out.println("El texto original es: " + new String(Base64.decodeBase64(cadena1Base64), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
}
