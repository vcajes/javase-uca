package py.edu.uca.javase.clase11;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.codec.digest.DigestUtils;

public class EjemploMessageDigests {

    public static void main(String[] args){
    	//MD5 y SHA de una cadena de texto
    	String cadena2 = "texto a hashear";
    	System.out.println("El MD5 de la cadena es: " + DigestUtils.md5Hex(cadena2));
    	System.out.println("El SHA256 de la cadena es: " + DigestUtils.sha256Hex(cadena2));

    	//MD5 de un archivo
    	try {
    		FileInputStream fis = new FileInputStream(new File("datos.txt"));
        	String md5DatosTxt = DigestUtils.md5Hex(fis);
        	System.out.println("El MD5 del archivo datos.txt es: " + md5DatosTxt);
    		fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}
