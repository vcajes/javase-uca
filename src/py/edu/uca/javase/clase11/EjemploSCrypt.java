package py.edu.uca.javase.clase11;

import com.lambdaworks.crypto.SCryptUtil;

public class EjemploSCrypt {

	public static void main(String[] args) {
		String originalPassword = "password";
		
		/*
		 * @param passwd    Password.
	     * @param N         CPU cost parameter.
	     * @param r         Memory cost parameter.
	     * @param p         Parallelization parameter.
	     */
        String generatedSecuredPasswordHash = SCryptUtil.scrypt(
        		originalPassword, (int)Math.pow(2, 12), 16, 16);
        System.out.println(generatedSecuredPasswordHash);
        //$s0$c1010$QKkpoazADz6UFXIEaus6+w==$dtNg+D0vGms7Y1ghhVkUHShzmDLfK83yq2ZSW6q78Wo=
        
        //comparamos con la contraseña original
        boolean matched = SCryptUtil.check("password", generatedSecuredPasswordHash);
        System.out.println(matched); //true
         
        //comparamos con una contraseña incorrecta
        matched = SCryptUtil.check("passwordno", generatedSecuredPasswordHash);
        System.out.println(matched); //false   
	}
}
