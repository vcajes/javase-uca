package py.edu.uca.javase.clase11;

import java.security.InvalidKeyException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.SecretKeySpec;

public class EjemploEncriptarSimetricaTexto {
	static String[] algorithms = new String[]{"RC2", "Blowfish", "DES", "DESede"};

	public static void main(String[] args) throws Exception {
		String algorithm = algorithms[0]; //eligimos el algoritmo a utilizar para encriptar
		
		//generamos la clave
		SecretKeySpec symKey = new SecretKeySpec("password".getBytes("UTF-8"), algorithm);

		//encriptamos
		Cipher c = Cipher.getInstance(algorithm);
		byte[] encryptionBytes = encryptF("texttoencrypt", symKey, c);
		
		System.out.println("El texto original es: texttoencrypt");
		System.out.println("El texto encriptado es: " + new String(encryptionBytes, "UTF-8"));
		System.out.println("El texto desencriptado es: " + decryptF(encryptionBytes, symKey, c)); //desencriptamos
		System.out.println("La clave utilizada para encriptar el texto fue: " + new String(symKey.getEncoded(), "UTF-8"));
	}
	
	private static byte[] encryptF(String input, SecretKeySpec pkey, Cipher c)
	throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
		c.init(Cipher.ENCRYPT_MODE, pkey);
		return c.doFinal(input.getBytes());
	}
	
	private static String decryptF(byte[] encryptionBytes, SecretKeySpec pkey, Cipher c)
	throws InvalidKeyException,	BadPaddingException, IllegalBlockSizeException{
		c.init(Cipher.DECRYPT_MODE, pkey);
		String decrypted = new String(c.doFinal(encryptionBytes));
		return decrypted;
	}
}

