package py.edu.uca.javase.clase04;

import java.lang.reflect.Method;

public class AppEjemploReflexiones{
 
	public static void main(String[] args) {
 		//Sin parametros
		Class noparams[] = {};
	 
		//Parametro String
		Class[] paramString = new Class[1];	
		paramString[0] = String.class;
	 
		//Parametro int
		Class[] paramInt = new Class[1];	
		paramInt[0] = Integer.TYPE;
	 
		try{
		    //Carga la clase AppClass
			Class cls = Class.forName("py.edu.uca.javase.clase03.reflexiones.AppClass");
			Object obj = cls.newInstance();
	 
			//llama al metodo printIt
			Method method = cls.getDeclaredMethod("printIt", noparams);
			method.invoke(obj, null);
	 
			//llama al metodo printItString y le pasa un String como parametro
			method = cls.getDeclaredMethod("printItString", paramString);
			method.invoke(obj, new String("mkyong"));
	 
			//llama al metodo printItInt y le pasa un int como parametro
			method = cls.getDeclaredMethod("printItInt", paramInt);
			method.invoke(obj, 123);
	 
			////llama al metodo setCounter y le pasa un int como parametro
			method = cls.getDeclaredMethod("setCounter", paramInt);
			method.invoke(obj, 999);
	 
			////llama al metodo printCounter y no le pasa ningun parametro
			method = cls.getDeclaredMethod("printCounter", noparams);
			method.invoke(obj, null);
	 
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
}
