package py.edu.uca.javase.clase04;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;

public class EjemploColecciones {

	public static void main(String[] args) {
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		map.put("uno", 1); //agregamos con key uno el valor 1
		map.put("dos", 2); //agragamos con el key dos el valor 2
		map.put("tres", 3); //agragamos con el key tres el valor 3
		map.put("cuatro", 4); //agragamos con el key cuatro el valor 4
		
		//imprimimos los elementos del map
		System.out.println(map);
		
		//eliminamos un elemento del map
		map.remove("dos");
		System.out.println(map);
		
		Integer val = map.get("no_existe");
		if(val == null){
			System.out.println( "El HashMap no tiene el key 'no_existe'" );
		}else{
			System.out.println( "El HashMap con el key 'no_existe' tiene el valor: " + val );
		}
		System.out.println( "El HashMap con el key 'uno' tiene el valor: " + map.get("uno")); //retorna 1
		
		//iteramos en todos los elementos (key, value) del mapa
		for (Map.Entry<String, Integer> entry : map.entrySet()){
			String key = entry.getKey();
			Integer value = entry.getValue();
			System.out.println("key=" + key + ", value=" + value);
		}
	}
		
	public static void hashSets(){
		HashSet<Integer> set = new HashSet<Integer>();
		set.add( new Integer( 6 ) );
		set.add( new Integer( 1 ) );
		set.add( new Integer( 4 ) );
		System.out.println( set ); //no hay orden
		
		//verificamos si el conjunto ya tiene el valor 1
		if(set.contains(new Integer(1))){
			System.out.println( "El conjunto ya tiene el numero 1." );
		}

		System.out.println( "No se pueden agregar duplicados." );
		Object value = set.add( new Integer( 8 ) );
		if ( value != null )
			System.out.println( "No se puede agregar el 8." );
		else{
			System.out.println( "Agregado el 8" );
			System.out.println( "El nuevo conjunto es " + set );
		}

		value = set.add( new Integer( 4 ) );
		if ( value != null )
			System.out.println( "No se puede agregar el 4." );
		else{
			System.out.println( "Agregado el 4." );
			System.out.println( "El nuevo conjunto es " + set );
		}
	}
	
	public static void linkedList(){
		LinkedList<String> linkedList = new LinkedList<String>();
		linkedList.add("Amy"); //agregamos un elemento
		linkedList.add("Bob"); //agregamos un elemento
		linkedList.add("Carl"); //agregamos un elemento
		Iterator<String> iter = linkedList.iterator();
		String first = iter.next(); //primer elemento
		String second = iter.next(); //segundo elemento
		iter.remove(); //eliminar el segundo elemento
		
		//Problemas de concurrencia
		ListIterator<String> iter1 = linkedList.listIterator();
		ListIterator<String> iter2 = linkedList.listIterator();
		iter1.next();
		iter1.remove();
		//iter2.next(); // throws ConcurrentModificationException
		
		//utilizar .get(indice) con listas enlazadas es muy lento
		linkedList.get(0); //obtiene el primer elemento
	}
	
	public static void arrayList(){
		ArrayList<String> arrayList = new ArrayList<String>(20); //20 elementos inicialmente
		arrayList.add("hola"); //se agrega al final de la lista
		arrayList.add("chau"); //se agrega al final de la lista
		arrayList.add(1, "medio"); //para agregar en la posicion 1
		
		String elem1 = arrayList.get(1); //obtiene el valor "medio"
		int posicionHola = arrayList.indexOf("hola"); //retorna 0
		
		//iterar en todos los elementos con for-each
		for(String item : arrayList){
			System.out.print(item + " - ");
		}
		System.out.println("");
		
		//iterar en todos los elementos con un Iterador
		Iterator<String> iterador = arrayList.iterator();
		while(iterador.hasNext()){ //mientras tenga mas elementos
			System.out.print(iterador.next() + " - ");
		}
		System.out.println("");
		
		//ordenar el arrayList
		Collections.sort(arrayList);
		System.out.println(arrayList);
	}
}
