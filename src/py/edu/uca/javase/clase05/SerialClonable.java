package py.edu.uca.javase.clase05;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerialClonable implements Cloneable, Serializable {

	private static final long serialVersionUID = 1L;

	@Override
	public Object clone(){
		try{
			//Primero nos guardamos a nosotros mismos (this) en un OutputStream en memoria
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			ObjectOutputStream out = new ObjectOutputStream(bout);
			out.writeObject(this);
			out.close();
			
			//Cargamos ese output stream al Object input stream para crear un objeto nuevo copiado
			ByteArrayInputStream bin = new ByteArrayInputStream(bout.toByteArray());
			ObjectInputStream in = new ObjectInputStream(bin);
			Object ret = in.readObject();
			in.close();
			
			return ret;
		}catch(Exception e){
			System.out.println("Ocurrio un error al clonar el objeto.");
			e.printStackTrace();
		}
		
		return null;
	}
}
