package py.edu.uca.javase.clase05;

public class EmpleadoClonable extends SerialClonable{
	
	private static final long serialVersionUID = 1L;
	
	protected String nombre;
	protected double salario;
	
	public EmpleadoClonable(){
		this.nombre = null;
		this.salario = 0;
	}
	
	public EmpleadoClonable(String nombre, double salario){
		this.nombre = nombre;
		this.salario = salario;
	}
	
	protected void setNombre(String nombre){
		this.nombre = nombre;
	}
	
	protected String getNombre(){
		return this.nombre;
	}
	
	protected void setSalario(double salario){
		this.salario = salario;
	}
	
	protected double getSalario(){
		return this.salario;
	}
	
	protected double getSalarioTotal(){
		return this.salario - this.salario*0.09;
	}
	
	@Override
	public String toString(){
		return String.format("Nombre: %s | Salario: %.2f", nombre, salario);
	}
}

