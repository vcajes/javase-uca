package py.edu.uca.javase.clase05;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import py.edu.uca.javase.clase03.Empleado;

public class JacksonSerializer {
	
	public static void main(String[] args) {
		Empleado emp = new Empleado("juan", 15000.0);
		
		//creamos el serializador/deserializador
		ObjectMapper serializer = new ObjectMapper();
		
		//permitimos serializar todos los miembros que no son publicos tambien
		serializer.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
		
		String stringJsonEmpleado = null;
		try {
			stringJsonEmpleado = serializer.writeValueAsString(emp); //serializamos
			System.out.println(stringJsonEmpleado);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		//deserializamos el String Json a un Objeto Java
		try {
			Empleado emp2 = serializer.readValue(stringJsonEmpleado, Empleado.class); //deserializamos
			System.out.println(emp2); //imprimimos emp2.toString()
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
