package py.edu.uca.javase.clase05;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;

import py.edu.uca.javase.clase03.Gerente;

public class ArchivosDiscoDuro {

	public static void main(String[] args) {
		clonarObjetos();		
		//manejoArchivos();
		//serializacionObjetos();
		//lineasDeTexto();
		//byteStreams();
	}
	
	public static void manejoArchivos(){
		File file = new File("salida.txt");
		System.out.println(file.getAbsolutePath());
		System.out.println(file.exists());
		System.out.println(file.isDirectory());
		System.out.println(file.lastModified());
	}
	
	public static void clonarObjetos(){
		EmpleadoClonable e1 = new EmpleadoClonable("juan", 5000.0);
		EmpleadoClonable e2 = (EmpleadoClonable) e1.clone();
		
		//modificamos e1, y veremos como NO se modifica e2
		e1.setNombre("pepe"); e1.setSalario(9000.0);
		
		System.out.println(e1);
		System.out.println(e2);
	}
	
	public static void serializacionObjetos(){
		//Guardamos objetos en el archivo
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("objetos_serializados.dat"));
			Gerente g1 = new Gerente("juan", 5000.0, 2000.0);
			Gerente g2 = new Gerente("luis", 8000.0, 4000.0);
			oos.writeObject(g1);
			oos.writeObject(g2);
			oos.close();
			System.out.println("Se han guardado correctamente dos objetos gerente en el archivo.");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//Leemos los objetos guardados desde el archivo
				ObjectInputStream oos = null;
				try {
					System.out.println("Se va a leer dos objetos desde el archivo.");
					oos = new ObjectInputStream(new FileInputStream("objetos_serializados.dat"));
					while(true){ //tenemos que leer el archivo hasta que se lance la excepcion de EOF
						Gerente g = (Gerente) oos.readObject();
						System.out.println(g);
					}						
				} catch (ClassNotFoundException e2){
					e2.printStackTrace();
				} catch (EOFException e3){
					System.out.println("Se llego al final del archivo y no hay mas objetos por leer.");
					try {
						oos.close();  //cerramos el archivo para liberar recursos
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				}
	}
	
	public static void lineasDeTexto(){
		//lectura de lineas de texto
		try {
			BufferedReader br = new BufferedReader(new FileReader("datos.txt"));
			String linea = br.readLine();
			while(linea != null){ //cuando linea es NULL entonces se acabo el archivo
				System.out.println(linea);
				linea = br.readLine(); //leemos la siguiente linea
			}
			br.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		
		//escritura de lineas de texto
		try {
			boolean autoFlush = true;
			PrintWriter pw = new PrintWriter(new FileWriter("salida.txt"), autoFlush);
			pw.println("Agregamos esta linea al archivo de texto con un [ENTER] al final");
			pw.print("Esta seria la segunda linea sin un [ENTER] --> ");
			pw.printf("texto con formato %.2f", 7.894);
			pw.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
	}
	
	public static void byteStreams(){
		try {
			//lee desde el directorio de trabajo del usuario, y eso podemos obtener con:
			System.out.println("El path del directorio de trabajo es: " + System.getProperty("user.dir"));
			
			//leemos el archivo
			FileInputStream fin = new FileInputStream("datos.txt");
			int b1 = fin.read();
			System.out.println("El byte leido es: " + b1); //el numero 65 == valor ASCII de la letra A
			fin.close();
			
			//Usamos un DataInputStream para leer datos con tipos
			FileOutputStream fos = new FileOutputStream("salida.txt");
			fos.write(65); //va a escribir la letra A en el archivo
			fos.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
	}
}
