package py.edu.uca.javase.clase03;

public class Clase03 {

	public static void main(String[] args) {
		//Ejemplo de herencia
		Empleado e = new Empleado("juan", 10000.0);
		Gerente g = new Gerente("luis", 90000.0, 5000.0);
		System.out.println("El empleado " + e.getNombre() + " gana un salario total de " + e.getSalarioTotal());
		System.out.println("El gerente " + g.getNombre() + " gana un salario total de " + g.getSalarioTotal());
		
		//Ejemplo de polimorfimos
		Gerente g1 = new Gerente("gerente1", 10000.0, 5000.0);
		Empleado g2 = new Gerente("gerente2", 90000.0, 5000.0);
		
		g1.setBonus(60000.0); //esto es valido
		
		//pero al usar la variable polimorfica, no se puede
		//g2.setBonus(60000.0); //lanza un error de compilacion
		
		Gerente gerenteCasteado = (Gerente) g2;
		gerenteCasteado.setBonus(1000.0);
		System.out.println("El gerente " + gerenteCasteado.getNombre() + " tiene un bonus de " + gerenteCasteado.getBonus());
		
		//Ejemplo de instanceof
		if(e instanceof Empleado){
			System.out.println("e es una instancia de Empleado"); //verdadero
		}		
		if(e instanceof Gerente){
			System.out.println("e es una instancia de Gerente"); //falso
		}
		if(g1 instanceof Empleado){
			System.out.println("g1 es una instancia de Empleado"); //verdadero
		}		
		if(g1 instanceof Gerente){
			System.out.println("g1 es una instancia de Gerente"); //verdadero
		}
		if(g2 instanceof Empleado){
			System.out.println("g2 es una instancia de Empleado"); //verdadero
		}		
		if(g2 instanceof Gerente){
			System.out.println("g2 es una instancia de Gerente"); //verdadero
		}

		//Ejemplos con clases abstractas
		//Creamos una FiguraGeometrica instanciando en plena linea sus metodos abstractos
		FiguraGeometrica fg = new FiguraGeometrica() {
			@Override
			public double perimetro() {
				return 0;
			}
			
			@Override
			public double area() {
				return 0;
			}
		};
		fg.printDetails();
		
		//Polimorfirmo
		FiguraGeometrica fgc = new Circulo(2.0);
		fgc.printDetails();
		
		//Forma clasica de instanciacion
		Circulo c1 = new Circulo(2.0);
		c1.printDetails();
		
		//Utilizando la funcion de la interface LeerDesdeTeclado
		Circulo c2 = new Circulo();
		c2.printDetails();
	}

}


