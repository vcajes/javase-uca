package py.edu.uca.javase.clase03;

import java.io.Serializable;

public class Empleado implements Serializable{

	private static final long serialVersionUID = -8142016979193339290L;
	
	protected String nombre;
	protected double salario;
	
	public Empleado(){
		this.nombre = null;
		this.salario = 0;
	}
	
	public Empleado(String nombre, double salario){
		this.nombre = nombre;
		this.salario = salario;
	}
	
	protected void setNombre(String nombre){
		this.nombre = nombre;
	}
	
	protected String getNombre(){
		return this.nombre;
	}
	
	protected void setSalario(double salario){
		this.salario = salario;
	}
	
	protected double getSalario(){
		return this.salario;
	}
	
	protected double getSalarioTotal(){
		return this.salario - this.salario*0.09;
	}
	
	@Override
	public String toString(){
		return String.format("Nombre: %s | Salario: %.2f", nombre, salario);
	}
}

