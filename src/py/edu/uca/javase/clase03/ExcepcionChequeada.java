package py.edu.uca.javase.clase03;

public class ExcepcionChequeada extends Exception {

	private String message = null;
	 
    public ExcepcionChequeada() {
        super();
    }
 
    public ExcepcionChequeada(String message) {
        super(message);
        this.message = message;
    }
 
    @Override
    public String toString() {
        return message;
    }
 
    @Override
    public String getMessage() {
        return message;
    }
}
