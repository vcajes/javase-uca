package py.edu.uca.javase.clase03;

import java.util.Scanner;

import py.edu.uca.javase.clase03.FiguraGeometrica;

public class Circulo extends FiguraGeometrica implements LeerDesdeTeclado {

	private double radio;
	
	public Circulo(){
		this.setNombre("Circulo");
		this.solicitarValoresAlUsuario();
	}
	
	public Circulo(double radio){
		this.setNombre("Circulo");
		this.radio = radio;
	}
	
	@Override
	public double area() {
		return Math.PI * Math.pow(this.radio, 2);
	}

	@Override
	public double perimetro() {
		return 2 * Math.PI * radio;
	}

	@Override
	public void solicitarValoresAlUsuario() {
		Scanner in = new Scanner(System.in);
		System.out.print("Ingrese el radio del circulo:\t");
		this.radio = in.nextDouble();
		in.close();
	}
}

