package py.edu.uca.javase.clase03;

public class ExcepcionNoChequeada extends RuntimeException {

	private String message = null;
	 
    public ExcepcionNoChequeada() {
        super();
    }
 
    public ExcepcionNoChequeada(String message) {
        super(message);
        this.message = message;
    }
 
    @Override
    public String toString() {
        return message;
    }
 
    @Override
    public String getMessage() {
        return message;
    }
}
