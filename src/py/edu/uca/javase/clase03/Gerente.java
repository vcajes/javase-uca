package py.edu.uca.javase.clase03;

import java.io.Serializable;

public class Gerente extends Empleado implements Serializable{

	private static final long serialVersionUID = -7073540834181014914L;
	
	private double bonus;
	
	public Gerente(String nombre, double salario, double bonus){
		super(nombre, salario); //esta linea es opcional, y llama al constructor de la superclase Empleado
		this.bonus = bonus;
	}
	
	protected void setBonus(double bonus){
		this.bonus = bonus;
	}
	
	protected double getBonus(){
		return this.bonus;
	}
	
	@Override
	protected double getSalarioTotal(){
		return (this.salario + this.bonus) - (this.salario + this.bonus) * 0.09;
	}
	
	@Override
	public String toString(){
		return String.format("Nombre: %s | Salario: %.2f | Bonus: %.2f", nombre, salario, bonus);
	}
}
