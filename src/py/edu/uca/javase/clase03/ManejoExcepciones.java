package py.edu.uca.javase.clase03;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class ManejoExcepciones {

	public static void main(String[] args) {
		//sinManejarExcepcionNochequeada();
		//manejandoExcepcionNochequeada();
		/*try{
			relanzandoExcepcionNochequeada();
		}catch(ArithmeticException fnfe){
			fnfe.printStackTrace();
		}*/
		
		//sinManejarExcepcionChequeada();
		//manejandoExcepcionChequeada();		
		//necesariamente debemos manejar la excepcion chequeada relanzada
		/*try{
			relanzandoExcepcionChequeada();
		}catch(FileNotFoundException fnfe){
			fnfe.printStackTrace();
		}*/
		
		//Excepcion personalizada
		//relanzarExcepcionPersonalizada(-2); //va a dar un error de compilacion a menos que se ponga dentro de un try catch
		relanzarExcepcionNoChequeadaPersonalizada(-1); //no es necesario poner dentro de un try catch
	}
	
	private static void relanzarExcepcionNoChequeadaPersonalizada(int valor) throws ExcepcionNoChequeada{
		if(valor > 0)
			return;
		throw new ExcepcionNoChequeada("El valor debe ser mayor a cero.");
	}
	
	private static void relanzarExcepcionChequeadaPersonalizada(int valor) throws ExcepcionChequeada{
		if(valor > 0)
			return;
		throw new ExcepcionChequeada("El valor debe ser mayor a cero.");
	}
	
	private static void manejandoExcepcionChequeada(){
		try{
			File file = new File("c:\\NonExistentFile.txt");
			FileReader fileReader = new FileReader(file);
		}catch(FileNotFoundException fe){
			fe.printStackTrace();
		}		
	}
	
	private static void sinManejarExcepcionChequeada(){
		File file = new File("c:\\NonExistentFile.txt");
		
		// si no hacemos throws de la expcecion o no capturamos la excepcion
		//tenemos un error de compilacion
		//FileReader fileReader = new FileReader(file); //descomentar linea para verificar el error		
	}
	
	private static void relanzandoExcepcionChequeada() throws FileNotFoundException{
		File file = new File("c:\\NonExistentFile.txt");
		
		//como se relanza la excepcion, no tenemos un error de compilacion
		FileReader fileReader = new FileReader(file);
		
	}
		
	
	static void relanzandoExcepcionNochequeada() throws ArithmeticException{
		int p = 10 / 0;
		System.out.print("Esto nunca se va imprimir. p: " + p);
	}
	
	static void sinManejarExcepcionNochequeada(){
		int p = 10 / 0;
		System.out.print("Esto nunca se va imprimir. p: " + p);
	}

	static void manejandoExcepcionNochequeada(){
		try{
			int p = 10 / 0;
			System.out.print("El valor de p: " + p);
		}catch(ArithmeticException ae){
			System.out.println("Manejamos en este bloque el error de la division por cero.");
			ae.printStackTrace(); //podemos imprimir el error ocurrio con esta linea
		}
	}
}
