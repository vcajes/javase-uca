package py.edu.uca.javase.clase06;

import java.util.concurrent.locks.*;

public class Bank{
	private final double[] accounts;
	private Lock bankLock;
	private Condition sufficientFunds;
	
	public Bank(int n, double initialBalance){
		accounts = new double[n];
		for (int i = 0; i < accounts.length; i++)
			accounts[i] = initialBalance;
		bankLock = new ReentrantLock(); //definimos el objeto de bloqueo para acceso a variables compartidas
		sufficientFunds = bankLock.newCondition(); //creamos el objeto de condicion
	}

	public void transfer(int from, int to, double amount) throws InterruptedException{
		bankLock.lock();
		try	{			
			while (accounts[from] < amount) //verificamos que la cuenta tenga dinero
				sufficientFunds.await(); //y mientras no tenga lo suficiente, volvemos a esperar
			
			System.out.print(Thread.currentThread());
			accounts[from] -= amount;
			System.out.printf(" %10.2f de la cuenta %d a la %d", amount, from, to);
			accounts[to] += amount;
			System.out.printf(" Balance Total: %10.2f%n", getTotalBalance());
			
			sufficientFunds.signalAll(); //cada vez que una cuenta reciba dinero, enviamos la señal para desbloquear
		}
		finally	{ bankLock.unlock(); }
	}

	public double getTotalBalance(){
		bankLock.lock();
		try	{
			double sum = 0;
			for (double a : accounts)
				sum += a;
			return sum;
		}
		finally	{ bankLock.unlock(); }
	}
	public int size(){ return accounts.length; }
}
