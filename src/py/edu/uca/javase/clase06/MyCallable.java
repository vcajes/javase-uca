package py.edu.uca.javase.clase06;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
 
public class MyCallable implements Callable<String> {
 
    @Override
    public String call() throws Exception {
        Thread.sleep(1000);
        return Thread.currentThread().getName(); //retornamos el nombre del hilo
    }
     
    public static void main(String args[]){
        ExecutorService executor = Executors.newFixedThreadPool(10); //creamos un pool de hilos de tamaño 10
        List<Future<String>> list = new ArrayList<Future<String>>(); //Creamos una lista para todos los Futures que crearemos      
        Callable<String> callable = new MyCallable(); //Creamos una instancia de MyCallable
        for(int i=0; i< 100; i++){
            Future<String> future = executor.submit(callable); //agregamos un callable al pool de hilos para que se ejecute
            list.add(future); //agregamos el Future a la lista
        }
        for(Future<String> fut : list){
            try {
                //imprimimos el valor del retorno del Future
            	//notemos la espera en la consola
                //debido a que Future.get() debe esperar a que termine su callable para obtener el resultado
                System.out.println(new Date()+ "::"+fut.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }       
        executor.shutdown(); //detenemos el excecutor
    } 
}
