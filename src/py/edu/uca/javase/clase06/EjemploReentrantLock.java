package py.edu.uca.javase.clase06;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class EjemploReentrantLock{
	public static int[] accounts = new int[100]; //cuentas (variable global accesible por todos)
	private Lock bankLock = new ReentrantLock(); // ReentrantLock implements the Lock interface
	
	public void transferirDinero(int from, int to, int amount){
		bankLock.lock(); //bloqueamos para que solo una transferencia a la vez pueda ocurrir
		try{
			System.out.print(Thread.currentThread());
			accounts[from] -= amount;
			System.out.printf(" %10.2f desde la cuenta %d a la %d", amount, from, to);
			accounts[to] += amount;
			System.out.printf(" Balance Total: %10.2f%n", getBalanceTotal());
		}finally{
			bankLock.unlock(); //nunca debemos olvidarnos desbloquear el lock
		}
	}

	public static int getBalanceTotal(){
		int balance = 0;
		for(int acc : accounts)
			balance += acc;
		return balance;
	}	
}
