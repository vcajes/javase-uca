package py.edu.uca.javase.clase06;

public class EjemploRunnable implements Runnable {
    public void run() {
        for (int i = 0; i < 3; i++) {
            System.out.println("Hilo Hijo ");
            try {
                Thread.sleep(100);
            } catch (InterruptedException ie) {
                System.out.println("Hilo hijo interrumpido! " + ie);
            }
        }
        System.out.println("Hilo hijo terminado!");
    }

    public static void main(String[] args) {
        Thread t = new Thread(new EjemploRunnable());
        t.start();

        for (int i = 0; i < 3; i++) {
            System.out.println("Hilo principal");
            try {
                Thread.sleep(100);
            } catch (InterruptedException ie) {
                System.out.println("Hilo principal interrumpido! " + ie);
            }
        }
        System.out.println("Hilo principal terminado!");
    }
}
